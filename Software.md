# Downloads

* Microsoft Visual Studio Community 2017: [Click Here](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=15)
* Microsoft SQL Server 2017 Developer Edition: [Click Here](https://go.microsoft.com/fwlink/?linkid=853016)
* Microsoft SQL Server Management Studio 17: [Click Here](https://go.microsoft.com/fwlink/?linkid=875802)

## Microsoft Imagine

* Sign up for Imagine: [Go here](https://imagine.microsoft.com/en-us/account/Edit) and log in to your MNSU account and fill out the form. Enter `Minnesota State University, Mankato` in the "School Name" field.
* Microsoft Azure for Students: 
    * [Go here](https://imagine.microsoft.com/en-US/Catalog/Product/99) after signing up for Imagine and choose Register Now. This will get you the Azure for Students Starter subscription.
    * Then activate the full Azure for Students by [going here](https://azure.microsoft.com/en-us/free/students/). 
