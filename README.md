# Entity Framework C# Workshop

> **Entity Framework Command Line Sample Project** (from July 12th lecture): [Download here](https://bitbucket.org/fmillion/mnsu-ef-workshop/raw/f2f37915b1f4a6027decf4693b0c46cc50ae9609/EFSamples.zip)

Topics to cover:

* Software stack. [Download links are here.](Software.md)
* Using NuGet to add Entity Framework to a project
* Connecting to a database by generating an Entity Framework code-first model
* Using LINQ to retrieve data from the Entity Framework database
* Updating and deleting data in the database
* (Briefly) What happens if the database changes

## URLs

* Database server: `mnsu-ef-workshop.database.windows.net`
    * Username: `Workshop`
    * Password: `EntityFramework0718`
